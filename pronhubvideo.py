# import os
import requests
import re
from requests.adapters import HTTPAdapter
from enum import Enum
"""
注意：pronhub链接有时限，2小时左右，过期请更换

F12 -->
M:\\pronhub
1. 得到index-f1-v1-a1.m3u8的pronhub地址（主要目的要是获取timestamp和hash）
2. 通过index内容，得到seg数量
3. requests下载
4. 通过copy /b 命令合并已下载的.ts文件

todo：
1. 文件下载失败重下
2. 网站下载限制
3. 断点续传
<-- OVER！

PATH
TARGETFOLDER
    ts
        index-f1-v1-a1.m3u8
        seg-1-f1-v1-a1.ts?validfrom=1592849163&validto=1592856363&hdl=-1&hash=Ql1D%2BAktY0VYXjbTHxWjelF8Ga8%3D
        seg-2-f1-v1-a1.ts?validfrom=1592849163&validto=1592856363&hdl=-1&hash=Ql1D%2BAktY0VYXjbTHxWjelF8Ga8%3D
        ...
    combine.cmd
    combined.mp4（取决于链接中文件格式）

    https://ev-h.phncdn.com/hls/videos/202005/27/318257511/,720P_4000K,480P_2000K,240P_400K,_318257511.mp4.urlset/index-f1-v1-a1.m3u8?validfrom=1592898314&validto=1592905514&hdl=-1&hash=%2BrBIbj1jHkxPV04Yl91IU0zDt%2F8%3D

    https://dv-h.phncdn.com/hls/videos/202006/10/322320561/,1080P_4000K,720P_4000K,480P_2000K,240P_400K,_322320561.mp4.urlset/index-f1-v1-a1.m3u8?ttl=1592904803&l=0&hash=e7ceb70608176c332fc46082be0ea346

通过这行获取到视频标识
    <meta name="twitter:image" content="https://ci.phncdn.com/videos/202006/10/322320561/thumbs_30/(m=e0YHGgaaaa)(mh=qMCku2ReTuNrTPaS)9.jpg">
5部分
    # 有好几个服务器dv、ev
    https://dv-h.phncdn.com/hls/videos/
    202006/10/322320561/
    ,1080P_4000K,720P_4000K,480P_2000K,240P_400K,
    _322320561.mp4
    .urlset/index-f1-v1-a1.m3u8?ttl=1592904803&l=0&hash=e7ceb70608176c332fc46082be0ea346
"""


class REQUESTSTATE(Enum):
    TIMEOUT = 0         # 链接超时
    READTIMEOUT = 0     # 读取超时
    FAILED = 0          # 请求失败
    SUCCESSED = 1       # 请求成功


class PHV():
    def __init__(self):
        self.index_count = 0

    # 解析 index-f1-v1-a1.m3u8
    def before_merge(self, path, file_name):
        with open(path, 'r', encoding='utf-8') as f:
            text_list = f.readlines()
            files = []
            for index, i in enumerate(text_list):
                # 若没含有'#EX'
                if '#EX' not in i:
                    r = re.search(r'(index).*(?=\?)', i.replace("\n", ""), re.M | re.I)
                    files.append(r.group())
                    self.index_count = index
            f.close()
            # cmd
            shell_str = '+'.join(files)
            # +'\n'+'del *.ts'
            shell_str = 'copy /b ' + shell_str + f' {file_name}.mp4' + '\n' + 'del *.ts'
            return shell_str

    def write_to_file(self, cmdString, path):
        with open(path + "combined.cmd", 'w') as f:
            f.write(cmdString)
            f.close()

    def get_request(self, url, timeout=(5, 10)):
        session = requests.Session()
        # 设置重试次数，max_retries + 1 为最大重试次数
        session.mount('http://', HTTPAdapter(max_retries=3))
        session.mount('https://', HTTPAdapter(max_retries=3))
        try:
            # 这里timeout设置为元祖类型，表示设置(链接超时, 读取超时)
            res = session.get(url, timeout=timeout)
            print('getRequest success')
            return res
        except requests.exceptions.RequestException as e:
            print(e)
            return e

    def download(self, index_url, path, combinedfile_name):
        # 下载index文件
        index_r = self.get_request(index_url)
        index_path = path + "index-f1-v1-a1.m3u8"
        with open(index_path, "wb") as code:
            code.write(index_r.content)
        # 写cmd命令，用于合并多个ts文件
        cmd = self.before_merge(index_path, combinedfile_name)
        self.write_to_file(cmd, path)
        # 下载ts文件
        for i in range(1, self.index_count + 1):
            filename = f"seg-{i}-f1-v1-a1.ts"
            url = index_url.replace("index-f1-v1-a1.m3u8", filename)
            d = self.get_request(url)
            if d.status_code == 200:
                with open(path + filename, "wb") as code:
                    code.write(d.content)
                    print(filename)
            elif d == REQUESTSTATE.FAILED:
                # 这里应该跳过这个文件，等下载完之后将失败池中文件重新下载
                pass
